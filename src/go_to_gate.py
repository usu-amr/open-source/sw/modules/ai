# State machine for going through the gate
# Clears readings from PID and kinematic nodes to start at zero
# sets target depth and target to be level or lean
# once the target depth is reached the sub swims through the gate 
# node outputs that the target was reached and the sub has made it through the gate

#!/usr/bin/env python
from enum import Enum
from std_msgs.msg import Float32, Bool
import math
import utils

class go_to_gate:
    class Stages(Enum):
        wait = 1
        find_baseline = 2
        achieve_target = 3
        go_to_gate = 4
        surface = 5
        done = 6

    class State:
        # for find_baseline
        baseline_samples = 0
        time_last_baseline_sample = 0
        yaw_x = 0
        yaw_y = 0
        # baselines
        depth_base = 0
        # targets
        target_yaw = 0
        target_depth = 0
        # for achieve_target
        did_set_target = False
        # for go_to_gate
        did_set_forward = False
        time_swim_start = 0
        # for paused and done and waiting
        time_waiting = 0
        time_paused = 0
        time_done = 0

    stage = None
    state = None
    globalState = None
    publishers = None
    params = None
    logger = None

    #initialization
    def __init__(self, logger, params, publishers, state):
        self.logger = logger
        self.params = params
        self.publishers = publishers
        self.globalState = state
        self.state = go_to_gate.State()

    # Reset variables and Enable side effects here
    def start(self):
        self.logger.loginfo('Starting AI')
        # reset state and stage to default
        self.stage = go_to_gate.Stages.wait
        self.state = go_to_gate.State()
        # start pid and kin nodes
        self.publishers.enable_pid_pub.publish(Bool(True))
        self.publishers.enable_kin_pub.publish(Bool(True))

    # Disable PID control and kinematic node here  
    def stop(self):
        self.logger.loginfo('Stopping AI')
        # disable control in pid node
        self.publishers.set_active_yaw_pub.publish(Bool(False))
        self.publishers.set_active_pitch_pub.publish(Bool(False))
        self.publishers.set_active_roll_pub.publish(Bool(False))
        self.publishers.set_active_up_pub.publish(Bool(False))
        self.publishers.set_forward_pub.publish(Float32(0))
        # stop pid and kinematic nodes
        self.publishers.enable_pid_pub.publish(Bool(False))
        self.publishers.enable_kin_pub.publish(Bool(False))

    # Do go to gate script here
    def loop(self, t_plus):
        #waiting time of 5 seconds before going to gate
        if self.stage == go_to_gate.Stages.wait:
            if t_plus > self.state.time_waiting + 1:
                self.state.time_waiting = t_plus
                self.logger.loginfo('Waiting to start')
            if t_plus > 5:
                # give 5 seconds for things to settle
                self.stage = go_to_gate.Stages.find_baseline

        #finds baseline before go to gate
        if self.stage == go_to_gate.Stages.find_baseline:
            # takes a sample every 0.5 seconds 
            # baseline assumes all heading within 180*
            if t_plus > self.state.time_last_baseline_sample + 0.5:
                self.logger.loginfo('Taking baseline sample')
                self.state.time_last_baseline_sample = t_plus
                self.state.baseline_samples += 1
                #get yaw 
                self.state.yaw_x += math.cos(self.globalState.yaw*math.pi/180)/10
                self.state.yaw_y += math.sin(self.globalState.yaw*math.pi/180)/10
                self.logger.loginfo('Yaw: '+str(self.state.yaw_x )+','+str(self.state.yaw_y)+','+str(self.globalState.yaw))
                #get depth
                self.state.depth_base += self.globalState.depth / 10

            # wait until we have 10 samples before showing the baseline yaw and depth
            if self.state.baseline_samples >= 10:
                self.state.target_yaw = math.atan2(self.state.yaw_y, self.state.yaw_x)*180/math.pi
                self.logger.loginfo('Baseline sample done, depth: ' + str(self.state.depth_base) + ', yaw: ' + str(self.state.target_yaw))
                #targets 800 PA depth
                self.state.target_depth = self.state.depth_base + 800 # dive to +800 Pa
                self.logger.loginfo('Targeting depth: ' + str(self.state.target_depth) + ', Targeting yaw: ' + str(self.state.target_yaw))
                self.stage = go_to_gate.Stages.achieve_target

        #Level the sub and target depth 
        if self.stage == go_to_gate.Stages.achieve_target:
            # set targets in the pid node once
            if not self.state.did_set_target:
                self.state.did_set_target = True
                self.logger.loginfo('Going to targets')
                # enable yaw control to target our yaw
                self.publishers.set_target_yaw_pub.publish(self.state.target_yaw)
                self.publishers.set_active_yaw_pub.publish(Bool(True))
                # enable pitch control to be level
                self.publishers.set_target_pitch_pub.publish(Float32(0))
                self.publishers.set_active_pitch_pub.publish(Bool(True))
                # enable roll control to be level
                self.publishers.set_target_roll_pub.publish(Float32(0))
                self.publishers.set_active_roll_pub.publish(Bool(True))
                # enable depth control to target our depth
                self.publishers.set_target_up_pub.publish(Float32(self.state.target_depth))
                self.publishers.set_active_up_pub.publish(Bool(True))
                self.publishers.const_up_force.publish(Float32(-55.0))

            # wait until we get in range of +-100 Pa in depth and 5* yaw of target
            if abs(self.globalState.depth - self.state.target_depth) < 100 and abs(utils.rot_diff(self.globalState.yaw, self.state.target_yaw)) < 5:
                self.logger.loginfo('Achieved Target')
                #Move forward to gate
                self.stage = go_to_gate.Stages.go_to_gate

        if self.stage == go_to_gate.Stages.go_to_gate:
            # set targets in the pid node once
            if not self.state.did_set_forward:
                self.state.did_set_forward = True
                self.publishers.set_forward_pub.publish(Float32(40))
                self.state.time_swim_start = t_plus
                #prints target time that sub began swimming forward to gate
                self.logger.loginfo('Swim started at T+ ' + str(round(self.state.time_swim_start, 2)))

            # waits for 60 sec to make it completely through the gate
            if t_plus > self.state.time_swim_start + 30:
                self.logger.loginfo('Swim stopped at T+ ' + str(round(t_plus, 2)))
                self.logger.loginfo('Stopping everything')
                # disable control in pid node
                self.publishers.set_active_yaw_pub.publish(Bool(False))
                self.publishers.set_active_pitch_pub.publish(Bool(False))
                self.publishers.set_active_roll_pub.publish(Bool(False))
                self.publishers.set_active_up_pub.publish(Bool(False))
                self.publishers.set_forward_pub.publish(Float32(0))
                # stop pid and kin nodes
                self.publishers.enable_pid_pub.publish(Bool(False))
                self.publishers.enable_kin_pub.publish(Bool(False))
                self.stage = go_to_gate.Stages.done

        if self.stage == go_to_gate.Stages.done:
            # Log that the sub is done going through the gate
            if t_plus > self.state.time_done + 5:
                self.state.time_done = t_plus
                self.logger.loginfo('Done')

    #Configure logging
    def paused(self, t_plus):
        # Log that the script is paused
        if t_plus > self.state.time_paused + 5:
            self.state.time_paused = t_plus
            self.logger.loginfo('Paused')