#subscribes to: yaw killswitch and pressure
#gets parameters: ai_script go_to_gate and ai_params
#
#This node is for the AI state machine main node. 
#it logs the AI status (start/stop) and handles publishing and subscribing

#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32, Bool
import math
import importlib

class AIState:
    is_stopped = True
    depth = 0
    yaw = 0

state = AIState()

def on_kill_switch(data):
    state.is_stopped = data.data

def on_yaw(data):
    state.yaw = data.data

def on_depth(data):
    state.depth = data.data

def node():

    class Logger:
        loginfo = None

        def __init__(self, parent):
            self.loginfo = parent.loginfo

    class Publishers:   
        enable_pid_pub = None
        enable_kin_pub = None
        set_forward_pub = None
        set_right_pub = None
        set_up_pub = None
        set_active_yaw_pub = None
        set_active_up_pub = None
        set_target_yaw_pub = None
        set_target_up_pub = None
        const_down_force = None

        def __init__(self, rospy):
            # enable nodes
            self.enable_pid_pub = rospy.Publisher('/pid_enable', Bool, queue_size=1)
            self.enable_kin_pub = rospy.Publisher('/enable_accel', Bool, queue_size=1)
            # static forces
            self.set_forward_pub = rospy.Publisher('/force_forward', Float32, queue_size=1)
            self.set_right_pub = rospy.Publisher('/force_right', Float32, queue_size=1)
            self.set_up_pub = rospy.Publisher('/force_up', Float32, queue_size=1)
            # active dof enable
            self.set_active_yaw_pub = rospy.Publisher('/active_yaw', Bool, queue_size=1)
            self.set_active_pitch_pub = rospy.Publisher('/active_pitch', Bool, queue_size=1)
            self.set_active_roll_pub = rospy.Publisher('/active_roll', Bool, queue_size=1)
            self.set_active_up_pub = rospy.Publisher('/active_up', Bool, queue_size=1)
            # active dof target
            self.set_target_yaw_pub = rospy.Publisher('/target_yaw', Float32, queue_size=1)
            self.set_target_pitch_pub = rospy.Publisher('/target_pitch', Float32, queue_size=1)
            self.set_target_roll_pub = rospy.Publisher('/target_roll', Float32, queue_size=1)
            self.set_target_up_pub = rospy.Publisher('/target_up', Float32, queue_size=1)
            self.const_up_force = rospy.Publisher('/const_up_force', Float32, queue_size=1)

    rospy.init_node('ai')
    
    rospy.Subscriber("/kill_switch", Bool, on_kill_switch, queue_size=1)
    rospy.Subscriber("/yaw", Float32, on_yaw, queue_size=1)
    rospy.Subscriber("/pressure", Float32, on_depth, queue_size=1)

    script_name = rospy.get_param('/ai_script','go_to_gate')
    params = rospy.get_param('/ai_params',dict())
    ScriptClass = getattr(importlib.import_module(script_name), script_name)
    current_script = ScriptClass(Logger(rospy),  params, Publishers(rospy), state)

    rate = rospy.Rate(100) # 100hz
    t_plus = 0
    start_time = rospy.get_time()
    time_t_plus_log = 0
    time_enable_motors = 0
    
    was_just_stopped = False
    rospy.loginfo("Getting AI ready")
    while not rospy.is_shutdown():
        now = rospy.get_time()
        if now > time_enable_motors + 1:
            time_enable_motors = now
            # enable the motors every 1 sec

        # AI is stopped
        if state.is_stopped and not was_just_stopped:
            was_just_stopped = True
            current_script.stop()
            rospy.loginfo("AI Stopped")

        # AI is started 
        elif not state.is_stopped and was_just_stopped:
            was_just_stopped = False
            current_script.start()
            start_time = now
            rospy.loginfo("AI Starting")
        t_plus = now - start_time

        # AI stays stopped
        if not state.is_stopped:
            if t_plus > time_t_plus_log + 2:
                time_t_plus_log = t_plus
                rospy.loginfo('T+ ' + str(round(t_plus, 2)))
            current_script.loop(t_plus)
        else:
            current_script.paused(t_plus)
        rate.sleep()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
