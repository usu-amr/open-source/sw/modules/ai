# this is a test script to perform each movement for 5 seconds 
# tests yaw, pitch, roll and move
# logs out the results of the test (success/failure)


#!/usr/bin/env python
from enum import Enum
from std_msgs.msg import Float32, Bool
import math

class test:
    #defines states total of 16
    class Stages(Enum):
        wait = 1
        yaw_n = 2
        yaw_e = 3
        yaw_s = 4
        yaw_w = 5
        pitch_up = 6
        pitch_down = 7
        roll_right = 8
        roll_left = 9
        move_front = 10
        move_right = 11
        move_back = 12
        move_left = 13
        move_up = 14
        move_down = 15
        done = 16

    #defines status states
    class State:
        time_waiting = 0
        time_paused = 0
        time_done = 0
        time_start_stage = 0
        stage_start = True

    #initializes to nothing
    stage = None
    state = None
    globalState = None
    publishers = None
    params = None
    logger = None

    def __init__(self, logger, params, publishers, state):
        self.logger = logger
        self.params = params
        self.publishers = publishers
        self.globalState = state
        self.state = test.State()

    # Reset variables and Enable side effects here
    def start(self):
        self.logger.loginfo('Starting AI')
        # reset the states and stages 
        self.stage = test.Stages.wait
        self.state = test.State()
        # start pid and kin nodes
        self.publishers.enable_pid_pub.publish(Bool(True))
        self.publishers.enable_kin_pub.publish(Bool(True))

    # Disable side effects here  
    def stop(self):
        self.logger.loginfo('Stopping AI')
        # disable control in pid node
        self.publishers.set_active_yaw_pub.publish(Bool(False))
        self.publishers.set_active_pitch_pub.publish(Bool(False))
        self.publishers.set_active_roll_pub.publish(Bool(False))
        self.publishers.set_active_up_pub.publish(Bool(False))
        self.publishers.set_forward_pub.publish(Float32(0))
        self.publishers.set_right_pub.publish(Float32(0))
        self.publishers.set_up_pub.publish(Float32(0))
        # stop pid and kin nodes
        self.publishers.enable_pid_pub.publish(Bool(False))
        self.publishers.enable_kin_pub.publish(Bool(False))

    # begin state script 
    def loop(self, t_plus):
        if self.stage == test.Stages.wait:

            #wait 5 seconds for things to calm and normalize
            if t_plus > self.state.time_waiting + 1:
                self.state.time_waiting = t_plus
                self.logger.loginfo('Waiting to start')
            if t_plus > 5:
                #after 5 seconds move foward
                self.stage = test.Stages.yaw_n
        
        # give 5 sec to point yaw (North)
        if self.stage == test.Stages.yaw_n:
            if self.state.stage_start:
                self.logger.loginfo('Yaw: N')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_yaw_pub.publish(Float32(0))
                self.publishers.set_active_yaw_pub.publish(Bool(True))
            #after 5 seconds yaw has been pointed to 
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_yaw_pub.publish(Bool(False))
                self.stage = test.Stages.yaw_e
            
        # give 5 sec to point East
        if self.stage == test.Stages.yaw_e:
            if self.state.stage_start:
                self.logger.loginfo('Yaw: E')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_yaw_pub.publish(Float32(270))
                self.publishers.set_active_yaw_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_yaw_pub.publish(Bool(False))
                self.stage = test.Stages.yaw_s

        # give 5 sec to point South
        if self.stage == test.Stages.yaw_s:
            if self.state.stage_start:
                self.logger.loginfo('Yaw: S')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_yaw_pub.publish(Float32(180))
                self.publishers.set_active_yaw_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_yaw_pub.publish(Bool(False))
                self.stage = test.Stages.yaw_w

        # give 5 sec to point West
        if self.stage == test.Stages.yaw_w:
            if self.state.stage_start:
                self.logger.loginfo('Yaw: W')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_yaw_pub.publish(Float32(90))
                self.publishers.set_active_yaw_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_yaw_pub.publish(Bool(False))
                self.stage = test.Stages.pitch_up

        # give 5 sec to pitch up
        if self.stage == test.Stages.pitch_up:
            if self.state.stage_start:
                self.logger.loginfo('Pitch: up')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_pitch_pub.publish(Float32(20))
                self.publishers.set_active_pitch_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_pitch_pub.publish(Bool(False))
                self.stage = test.Stages.pitch_down

        # give 5 sec to pitch down
        if self.stage == test.Stages.pitch_down:
            if self.state.stage_start:
                self.logger.loginfo('Pitch: down')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_pitch_pub.publish(Float32(-20))
                self.publishers.set_active_pitch_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_pitch_pub.publish(Bool(False))
                self.stage = test.Stages.roll_right

        # give 5 sec to roll right
        if self.stage == test.Stages.roll_right:
            if self.state.stage_start:
                self.logger.loginfo('Pitch: right')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_roll_pub.publish(Float32(20))
                self.publishers.set_active_roll_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_roll_pub.publish(Bool(False))
                self.stage = test.Stages.roll_left

        # give 5 sec to roll left
        if self.stage == test.Stages.roll_left:
            if self.state.stage_start:
                self.logger.loginfo('Pitch: left')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_target_roll_pub.publish(Float32(-20))
                self.publishers.set_active_roll_pub.publish(Bool(True))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_active_roll_pub.publish(Bool(False))
                self.stage = test.Stages.move_front

        # give 5 sec to move front
        if self.stage == test.Stages.move_front:
            if self.state.stage_start:
                self.logger.loginfo('Move: front')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_forward_pub.publish(Float32(20))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_forward_pub.publish(Float32(0))
                self.stage = test.Stages.move_right

        # give 5 sec to move right
        if self.stage == test.Stages.move_right:
            if self.state.stage_start:
                self.logger.loginfo('Move: right')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_right_pub.publish(Float32(20))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_right_pub.publish(Float32(0))
                self.stage = test.Stages.move_back

        # give 5 sec to move back
        if self.stage == test.Stages.move_back:
            if self.state.stage_start:
                self.logger.loginfo('Move: back')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_forward_pub.publish(Float32(-20))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_forward_pub.publish(Float32(0))
                self.stage = test.Stages.move_left

        # give 5 sec to move left
        if self.stage == test.Stages.move_left:
            if self.state.stage_start:
                self.logger.loginfo('Move: left')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_right_pub.publish(Float32(-20))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_right_pub.publish(Float32(0))
                self.stage = test.Stages.move_up

        # give 5 sec to move up
        if self.stage == test.Stages.move_up:
            if self.state.stage_start:
                self.logger.loginfo('Move: up')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_up_pub.publish(Float32(20))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_up_pub.publish(Float32(0))
                self.stage = test.Stages.move_down

        # give 5 sec to move down
        if self.stage == test.Stages.move_down:
            if self.state.stage_start:
                self.logger.loginfo('Move: done')
                self.state.stage_start = False
                self.state.time_start_stage = t_plus
                self.publishers.set_up_pub.publish(Float32(-20))
            if t_plus > self.state.time_start_stage + 5:
                self.state.stage_start = True
                self.publishers.set_up_pub.publish(Float32(0))
                self.stage = test.Stages.done

        if self.stage == test.Stages.done:
            # just log that we are done
            if t_plus > self.state.time_done + 5:
                self.state.time_done = t_plus
                self.logger.loginfo('Done')

    #Log stuff out here
    def paused(self, t_plus):
        # just log that we are paused
        if t_plus > self.state.time_paused + 5:
            self.state.time_paused = t_plus
            self.logger.loginfo('Paused')
