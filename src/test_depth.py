# takes a baseline sample for depth, targets depth and then goes to that depth
# used to test that depth targeting is funtional, logs results of the test

#!/usr/bin/env python
from enum import Enum
from std_msgs.msg import Float32, Bool
import math

class test_depth:
    class Stages(Enum):
        wait = 1
        find_baseline = 2
        achieve_target = 3
        done = 4

    class State:
        time_waiting = 0
        time_paused = 0
        time_done = 0
        baseline_samples = 0
        time_last_baseline_sample = 0
        depth_base = 0
        target_depth = 0
        did_set_target = False
        time_dive_start = 0

    stage = None
    state = None
    globalState = None
    publishers = None
    params = None
    logger = None

    def __init__(self, logger, params, publishers, state):
        self.logger = logger
        self.params = params
        self.publishers = publishers
        self.globalState = state
        self.state = test_depth.State()

    # Reset variables and Enable side effects here
    def start(self):
        self.logger.loginfo('Starting AI')
        # reset state and stage to default
        self.stage = test_depth.Stages.wait
        self.state = test_depth.State()
        # start pid and kin nodes
        self.publishers.enable_pid_pub.publish(Bool(True))
        self.publishers.enable_kin_pub.publish(Bool(True))

    # Disable side effects here  
    def stop(self):
        self.logger.loginfo('Stopping AI')
        # disable control in pid node
        self.publishers.set_active_pitch_pub.publish(Bool(False))
        self.publishers.set_active_roll_pub.publish(Bool(False))
        self.publishers.set_active_up_pub.publish(Bool(False))
        # stop pid and kin nodes
        self.publishers.enable_pid_pub.publish(Bool(False))
        self.publishers.enable_kin_pub.publish(Bool(False))

    # Do script here
    def loop(self, t_plus):
        if self.stage == test_depth.Stages.wait:
            if t_plus > self.state.time_waiting + 1:
                self.state.time_waiting = t_plus
                self.logger.loginfo('Waiting to start')
            if t_plus > 5:
                # give 5 seconds for things to settle
                self.stage = test_depth.Stages.find_baseline
        
        if self.stage == test_depth.Stages.find_baseline:
            # take a sample every 0.5 seconds
            if t_plus > self.state.time_last_baseline_sample + 0.5:
                self.logger.loginfo('Taking baseline sample')
                self.state.time_last_baseline_sample = t_plus
                self.state.baseline_samples += 1
                self.state.depth_base += self.globalState.depth / 10
                self.logger.loginfo('Depth ' + str(self.state.depth_base) + ' ' + str(self.globalState.depth))
            # wait until we have 10 samples
            if self.state.baseline_samples >= 10:
                self.logger.loginfo('Baseline sample done, depth: ' + str(self.state.depth_base))
                self.state.target_depth = self.state.depth_base + 800 # dive to +800 Pa
                self.logger.loginfo('Will target, depth: ' + str(self.state.target_depth))
                self.stage = test_depth.Stages.achieve_target

        if self.stage == test_depth.Stages.achieve_target:
            # set targets in the pid node once
            if not self.state.did_set_target:
                self.state.did_set_target = True
                self.state.time_dive_start = t_plus
                self.logger.loginfo('Going to targets')
                # enable pitch control to be level
                self.publishers.set_target_pitch_pub.publish(Float32(0))
                self.publishers.set_active_pitch_pub.publish(Bool(True))
                # enable roll control to be level
                self.publishers.set_target_roll_pub.publish(Float32(0))
                self.publishers.set_active_roll_pub.publish(Bool(True))
                # enable depth control to target our depth
                self.publishers.set_target_up_pub.publish(Float32(self.state.target_depth))
                self.publishers.const_up_force.publish(Float32(-55.0))
                self.publishers.set_active_up_pub.publish(Bool(True))
            # wait 15 sec at depth
            if t_plus > self.state.time_dive_start + 15:
                self.publishers.set_active_pitch_pub.publish(Bool(False))
                self.publishers.set_active_roll_pub.publish(Bool(False))
                self.publishers.set_active_up_pub.publish(Bool(False))
                self.stage = test_depth.Stages.done

        if self.stage == test_depth.Stages.done:
            # just log that we are done
            if t_plus > self.state.time_done + 5:
                self.state.time_done = t_plus
                self.logger.loginfo('Done')

    #Log stuff out here
    def paused(self, t_plus):
        # just log that we are paused
        if t_plus > self.state.time_paused + 5:
            self.state.time_paused = t_plus
            self.logger.loginfo('Paused')