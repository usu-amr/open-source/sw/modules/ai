import math

def mod(a, n):
    return a - math.floor(a / n) * n
    
def rot_diff(sourceA, targetA):
    diff = targetA - sourceA
    return (diff + 180) % 360 - 180